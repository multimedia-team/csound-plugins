Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: csound-plugins
Upstream-Contact: https://csound.com/community.html
Source: https://github.com/csound/plugins
Files-Excluded:
 *.exe
 *.dylib

Files: *
Copyright: © 1998-2022 Csound developers
License: LGPL-2.1+


Files: AbletonLinkOpcodes/ableton_link_opcodes.cpp
Copyright: 2017, Michael Gogins
  2013, Google Inc.
License: LGPL-2.1+ and BSD-3-clause
Comment:
 This file includes a verbatim copy of libjingle for the android builds
 which is (© Google Inc under the BSD-3-clause ).
 The rest is © Gogins under the LGPL-2.1+

Files: CUDA/*
Copyright: 2013-2014, Victor Lazzarini
 2014, Russell Bradford, Victor Lazzarini, John ffitch
License: LGPL-2.1+

Files: chua/*
Copyright: 2008, Michael Gogins
License: LGPL-2.1+

Files: cmake/Modules/FindEIGEN3.cmake
Copyright: 2006-2007, Montel Laurent, <montel@kde.org>
 2008-2009, Gael Guennebaud, <g.gael@free.fr>
 2009- Benoit Jacob <jacob.benoit.1@gmail.com>
License: BSD-2-clause

Files: faustcsound/*
Copyright: 2013, Victor Lazzarini
License: LGPL-2.1+

Files: fluidOpcodes/*
Copyright: 2003, Steven Yi
License: LGPL-2.1+

Files: hdf5/*
Copyright: 2014-2016, Edward Costello
License: LGPL-2.1+

Files: image/*
Copyright: 2007, Cesare Marilungo
License: LGPL-2.1+

Files: installer/win64/Installer.iss
Copyright: 2017, Rory Walsh
License: LGPL-2.1+

Files: jackops/*
Copyright: 2008, Cesare Marilungo
 2010, Michael Gogins
License: LGPL-2.1+

Files: linear_algebra/*
Copyright: 2008, Michael Gogins
License: LGPL-2.1+

Files: mp3/*
Copyright: 2019, John ffitch
License: LGPL-2.1+

Files: opencl/*
Copyright: 2014-2019, Michael Gogins
License: LGPL-2.1+

Files: p5glove/*
Copyright: 2009, John ffitch
License: LGPL-2.1+

Files: p5glove/libp5glove/*
Copyright: 2003-2004, Jason McMullan <ezrec@hotmail.com>
 2004, Ross Bencina <rossb@audiomulch.com>
 2004, Tim Kreger <tkreger@bigpond.net.au>
License: LGPL-2.1+

Files: py/*
Copyright: 2002, Maurizio Umberto Puxeddu
 2002-2004, Michael Gogins
 2019, François Pinot
License: LGPL-2.1+

Files: stk/*
Copyright: 2005, Michael Gogins
License: LGPL-2.1+

Files: websockets/*
Copyright: 2015-2016, Edward Costello.
License: LGPL-2.1+

Files: widgets/FL_graph.cpp
 widgets/winFLTK.c
Copyright: 2002-2003, John ffitch
License: LGPL-2.1+

Files: widgets/virtual_keyboard/*
Copyright: 2006, Steven Yi
License: LGPL-2.1+

Files: widgets/widgets.*
Copyright: 2002, Gabriel Maldonado
License: LGPL-2.1+

Files: widgets/winFLTK.h
Copyright: 2006, Istvan Varga
License: LGPL-2.1+

Files: wiimote/*
Copyright: 2009, John ffitch
License: LGPL-2.1+

Files: debian/*
Copyright: 2022-2024 IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>
License: LGPL-2.1+


License: LGPL-2.1+
 This software is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 This software is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 Some files differ from above by using other combinations of "this
 file", "this header", "this software, "this program" and "the Csound
 Library", but otherwise identical license and disclaimer.
 .
 On Debian systems, the complete text of the GNU Lesser General Public
 License (LGPL) version 2.1 can be found in
 '/usr/share/common-licenses/LGPL-2.1'.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

License: BSD-2-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE NETBSD FOUNDATION, INC. AND CONTRIBUTORS
 ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE FOUNDATION OR CONTRIBUTORS
 BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
  1. Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
  2. Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
  3. The name of the author may not be used to endorse or promote products
     derived from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
